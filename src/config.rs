use crate::error::*;
use crate::media_type::MediaType;
use serde::Deserialize;
use std::{
    env,
    fs::{self, File},
    io::{self, Read},
    net::SocketAddr,
    path::PathBuf,
};

#[derive(Debug, Deserialize, Clone)]
pub(crate) struct Config {
    pub(crate) net: NetConfig,
    pub(crate) database: DatabaseConfig,
}

#[derive(Debug, Deserialize, Clone)]
pub(crate) struct NetConfig {
    /// The address the server will listen to.
    pub(crate) address: SocketAddr,

    /// The server host. Used if the user uploads using the legacy upload API and no domain is provided.
    #[cfg(feature = "legacy_compat")]
    pub(crate) host: String,

    /// The TLS certificate file.
    pub(crate) cert_file: Option<PathBuf>,
    /// The TLS key file.
    pub(crate) key_file: Option<PathBuf>,
}

impl NetConfig {
    #[cfg(feature = "legacy_compat")]
    pub(crate) fn is_encrypted(&self) -> bool {
        self.cert_file.is_some() && self.key_file.is_some()
    }
}

#[derive(Debug, Deserialize, Clone)]
pub(crate) struct DatabaseConfig {
    /// The folder in which images are saved.
    pub(crate) directory: PathBuf,

    /// The folder from which legacy images are read.
    #[cfg(feature = "legacy_compat")]
    pub(crate) legacy_directory: PathBuf,

    /// A file containing a list of users and their api keys. Verification is turned off if this is `None`.
    pub(crate) user_file: Option<PathBuf>,
    /// Whether metadata files should contain the uploader key.
    /// If verification is turned off, no uploader will be written.
    pub(crate) write_uploader: bool,

    pub(crate) allowed_media_types: Vec<MediaType>,

    /// Maximum size per file in bytes.
    /// You can use strings like "5 MiB" or similar.
    #[serde(deserialize_with = "serde_humanize_rs::deserialize")]
    pub(crate) maximum_file_size: usize,
}

/// Reads the configuration file at `$CONFIG_PATH` or creates one if none was found.
pub(crate) fn read_config() -> Result<Config, Error> {
    let path = env::var("CONFIG_PATH").unwrap_or_else(|_| String::from("./upsolotl.toml"));
    let path = PathBuf::from(path);

    match File::open(&path) {
        Ok(mut file) => {
            let mut input = String::new();
            file.read_to_string(&mut input)?;
            Ok(toml::from_str(&input)?)
        }
        Err(ref err) if err.kind() == io::ErrorKind::NotFound => {
            let config = default_config();
            fs::write(path, &config)?;
            Ok(toml::from_str(&config).expect("The default configuration file is invalid :/"))
        }
        Err(err) => Err(Error::Io(err)),
    }
}

fn default_config() -> String {
    let mut cfg = String::new();
    cfg.push_str(
        r#"# This is the upsolotl configuration file.

[net]
# The address to which the server should be bound.
address = "0.0.0.0:8080"
"#,
    );

    if cfg!(feature = "legacy_compat") {
        cfg.push_str(
            r#"
# The default host name which may be sent if the `legacy_compat` feature is enabled.
host = "example.com"
"#,
        );
    }

    cfg.push_str(
        r#"
# The certificate file to use for HTTPS.
# Requires `key_file` to also be set.
# cert_file = ""

# The key file to use for HTTPS.
# Requires `cert_file` to also be set.
# key_file = ""

[database]
# The directory in which uploaded images will be stored.
# This folder shouldn't contain anything but the uploaded images.
directory = "images"
"#,
    );

    if cfg!(feature = "legacy_compat") {
        cfg.push_str(
            r#"
# The directory which will contain old images.
# Only used if the `legacy_compat` feature is enabled.
legacy_directory = "images_old"
"#,
        );
    }

    cfg.push_str(
        r#"
# This file contains all user keys.
# Remove this if you don't want any authentication.
# Users can be managed through the `add-key`, `remove-key` and `list-keys` subcommands.
user_file = "users.db"

# Whether the uploader key should be written to the image metadata.
# This might be useful if you have to check for illegal content and ban the user.
write_uploader = true

# How large images can be at most.
# Be sure to pick something that can fit into memory multiple times, as images aren't streamed yet.
maximum_file_size = "5 MiB"

# A list of allowed media types (specified in the "Content-Type" header in HTTP).
# If the content type isn't listed here, `application/octet-stream` will be used.
# Which means the file will simply be downloaded by the browser without executing anything.
# Only change this if you know what you are doing! A wrong configuration can be a security risk!
allowed_media_types = [
    "application/json",
    "application/octet-stream",
    "application/ogg",
    "application/vnd",
    "application/x-7z-compressed",
    "application/x-tar",
    "application/zip",
    "audio/aac",
    "audio/midi",
    "audio/mp4",
    "audio/mpeg",
    "audio/ogg",
    "audio/opus",
    "audio/wav",
    "audio/webm",
    "font/otf",
    "font/ttf",
    "font/woff",
    "font/woff2",
    "image/bmp",
    "image/gif",
    "image/jpeg",
    "image/png",
    "image/tiff",
    "image/vnd",
    "image/webp",
    "text/csv",
    "text/plain",
    "video/3gpp",
    "video/3gpp2",
    "video/mp2t",
    "video/mp4",
    "video/mpeg",
    "video/ogg",
    "video/webm",
    "video/x-msvideo",
]
"#,
    );

    cfg
}
