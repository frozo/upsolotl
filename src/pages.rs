use actix_multipart::Field;
use actix_multipart::Multipart;
use actix_web::{http::header, web, HttpRequest, HttpResponse, Responder};
use futures::{StreamExt, TryStreamExt};

use crate::config::Config;
use crate::database;
use crate::error::Error;
use crate::media_type::MediaType;
use crate::users::Users;

#[actix_web::post(r"/v1/api/upload")]
pub(crate) async fn upload(
    config: web::Data<Config>,
    users: web::Data<Option<Users>>,
    request: HttpRequest,
    mut payload: Multipart,
) -> Result<impl Responder, Error> {
    #[derive(serde::Serialize)]
    struct R {
        location: String,
    }

    let mut uploader = None;
    if let Some(users) = &**users {
        if let Some(value) = request.headers().get(header::AUTHORIZATION) {
            let header = value.to_str().map_err(|_| Error::AuthenticationFailed)?;
            let mut splits = header.splitn(2, " ");
            let type_ = splits.next().ok_or(Error::AuthenticationFailed)?;
            let key = splits.next().ok_or(Error::AuthenticationFailed)?;
            if type_ != "Key" || !users.is_verified(key) {
                return Err(Error::AuthenticationFailed);
            }
            if config.database.write_uploader {
                uploader = Some(key.to_string());
            }
        } else {
            return Err(Error::AuthenticationFailed);
        }
    }

    if let Ok(Some(field)) = payload.try_next().await {
        let media_type = find_compatible_media_type(&config, field.content_type().clone().into());
        let buf = read_field(field, config.database.maximum_file_size).await?;

        let hash = database::write(&config.database, media_type, uploader, buf).await?;
        let location = format!("/v1/{}", hash);
        Ok(HttpResponse::Ok().json(R { location }))
    } else {
        Err(Error::MissingFile)
    }
}

#[actix_web::get(r"/v1/{hash}")]
pub(crate) async fn view(
    config: web::Data<Config>,
    web::Path(hash): web::Path<String>,
) -> Result<impl Responder, Error> {
    let hash = hash.split(".").next().unwrap();
    let (content, metadata) = database::read(&config.database, &hash).await?;
    Ok(HttpResponse::Ok()
        .content_type(metadata.media_type.as_str())
        .body(content))
}

#[cfg(feature = "legacy_compat")]
#[actix_web::post(r"/upload")]
pub(crate) async fn legacy_upload(
    config: web::Data<Config>,
    users: web::Data<Option<Users>>,
    mut payload: Multipart,
) -> Result<impl Responder, Error> {
    use sodiumoxide::randombytes::randombytes_uniform;

    let mut media_type = None;
    let mut buf = None;
    let mut host = config.net.host.clone();
    let mut key = None;

    while let Ok(Some(field)) = payload.try_next().await {
        if let Some(dispo) = field.content_disposition() {
            match dispo.get_name() {
                Some("file") => {
                    let content_type = field.content_type().clone().into();
                    media_type = Some(find_compatible_media_type(&config, content_type));

                    buf = Some(read_field(field, config.database.maximum_file_size).await?);
                }
                Some("domains") => {
                    let buf = read_field(field, 1024).await?;
                    let mut domains: Vec<String> =
                        serde_json::from_slice(&buf).map_err(|_| Error::InvalidJson)?;
                    if !domains.is_empty() {
                        // choosing a domain using `sodiumoxide` to avoid an explicit dependency on `rand`
                        let idx = randombytes_uniform(domains.len() as u32) as usize;
                        // just a way of moving out the value from the vec
                        host = domains.swap_remove(idx);
                    }
                }
                Some("userKey") => {
                    let buf = read_field(field, 256).await?;
                    key = Some(String::from_utf8(buf).map_err(|_| Error::AuthenticationFailed)?);
                }
                Some("password") | Some("urlStyle") | Some("keepExtension") => {
                    // let's just ignore these
                }
                _ => return Err(Error::InvalidField),
            }
        } else {
            return Err(Error::InvalidField);
        }
    }

    if let (Some(users), Some(key)) = (&**users, &key) {
        if !users.is_verified(key) {
            return Err(Error::AuthenticationFailed);
        }
    } else {
        return Err(Error::AuthenticationFailed);
    }

    if let (Some(buf), Some(media_type)) = (buf, media_type) {
        if !config.database.write_uploader {
            key = None;
        }
        let hash = database::write(&config.database, media_type, key, buf).await?;
        let scheme = if config.net.is_encrypted() {
            "https"
        } else {
            "http"
        };
        let location = format!("{}://{}/v1/{}", scheme, host, hash);

        Ok(HttpResponse::Ok().body(location))
    } else {
        Err(Error::MissingFile)
    }
}

#[cfg(feature = "legacy_compat")]
#[derive(serde::Deserialize)]
pub(crate) struct LegacyViewQuery {
    key: String,
}

#[cfg(feature = "legacy_compat")]
#[actix_web::get(r"/{name}")]
pub(crate) async fn legacy_view_query(
    config: web::Data<Config>,
    web::Path(name): web::Path<String>,
    web::Query(q): web::Query<LegacyViewQuery>,
) -> Result<impl Responder, Error> {
    let (content, mut media_type) = database::read_legacy(&config.database, &name, q.key).await?;
    media_type = find_compatible_media_type(&config, media_type);

    Ok(HttpResponse::Ok()
        .content_type(media_type.as_str())
        .body(content))
}

#[cfg(feature = "legacy_compat")]
#[actix_web::get(r"/{password}/{name}")]
pub(crate) async fn legacy_view_param(
    config: web::Data<Config>,
    web::Path((password, name)): web::Path<(String, String)>,
) -> Result<impl Responder, Error> {
    let (content, mut media_type) =
        database::read_legacy(&config.database, &name, password).await?;
    media_type = find_compatible_media_type(&config, media_type);

    Ok(HttpResponse::Ok()
        .content_type(media_type.as_str())
        .body(content))
}

async fn read_field(mut field: Field, limit: usize) -> Result<Vec<u8>, Error> {
    let mut buf = Vec::new();
    while let Some(chunk) = field.next().await {
        let chunk = chunk?;
        if buf.len() + chunk.len() > limit {
            return Err(Error::FileTooLarge { max: limit });
        }
        buf.extend_from_slice(&chunk);
    }

    Ok(buf)
}

fn find_compatible_media_type(config: &Config, media_type: MediaType) -> MediaType {
    config
        .database
        .allowed_media_types
        .iter()
        .find(|mt| mt.is_compatible(&media_type))
        .cloned()
        .unwrap_or_default()
}
