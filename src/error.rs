use std::io;

use actix_web::middleware;
use actix_web::middleware::errhandlers::ErrorHandlerResponse;
use actix_web::{http::StatusCode, web, web::ServiceConfig, HttpResponse, ResponseError};

#[derive(Debug, thiserror::Error)]
pub(crate) enum Error {
    #[error("I/O: {0}")]
    Io(#[from] io::Error),
    #[error("TOML: {0}")]
    Toml(#[from] toml::de::Error),
    #[error("JSON: {0}")]
    Json(#[from] serde_json::Error),
    #[error("CBOR: {0}")]
    Cbor(#[from] serde_cbor::Error),
    #[cfg(feature = "legacy_compat")]
    #[error("OpenSSL: {0}")]
    OpenSSL(#[from] openssl::error::ErrorStack),
    #[error("Task was canceled")]
    TaskCanceled,
    #[error("You need to provide both a key and a certificate to accept TLS connections")]
    NeedKeyAndCert,
    #[error("Not Found")]
    NotFound,
    #[error("Invalid query")]
    InvalidQuery,
    #[error("Invalid or too long JSON data")]
    InvalidJson,
    #[error("Invalid or too long form")]
    InvalidForm,
    #[error("Multipart: {0}")]
    Multipart(#[from] actix_multipart::MultipartError),
    #[error("Unknown id")]
    UnknownId,
    #[error("Authentication failed")]
    AuthenticationFailed,
    #[error("Ciphertext didn't pass verification")]
    VerificationDidntPass,
    #[error("File missing from upload")]
    MissingFile,
    #[error("Can't exceed upload limit of {max} bytes")]
    FileTooLarge { max: usize },
    #[cfg(feature = "legacy_compat")]
    #[error("Multipart field isn't known")]
    InvalidField,
}

impl ResponseError for Error {
    fn error_response(&self) -> HttpResponse {
        #[derive(serde::Serialize)]
        struct R {
            message: String,
        }

        let message = self.to_string();
        log::info!("Error: {} ({:?})", message, self);
        HttpResponse::build(self.status_code()).json(R { message })
    }

    fn status_code(&self) -> StatusCode {
        match self {
            Error::NotFound => StatusCode::NOT_FOUND,
            Error::InvalidQuery => StatusCode::BAD_REQUEST,
            Error::InvalidJson => StatusCode::BAD_REQUEST,
            Error::InvalidForm => StatusCode::BAD_REQUEST,
            Error::Multipart(_) => StatusCode::BAD_REQUEST,
            Error::UnknownId => StatusCode::NOT_FOUND,
            Error::AuthenticationFailed => StatusCode::FORBIDDEN,
            Error::MissingFile => StatusCode::BAD_REQUEST,
            Error::FileTooLarge { .. } => StatusCode::PAYLOAD_TOO_LARGE,
            #[cfg(feature = "legacy_compat")]
            Error::InvalidField => StatusCode::BAD_REQUEST,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

pub(crate) fn configure(cfg: &mut ServiceConfig) {
    cfg.app_data(web::QueryConfig::default().error_handler(|_err, _| Error::InvalidQuery.into()))
        .app_data(web::PathConfig::default().error_handler(|_err, _| Error::NotFound.into()))
        .app_data(web::JsonConfig::default().error_handler(|_err, _| Error::InvalidJson.into()))
        .app_data(web::FormConfig::default().error_handler(|_err, _| Error::InvalidForm.into()));
}

pub(crate) fn handle_status_code<B: 'static>(
    status: StatusCode,
    error: impl Fn() -> Error + 'static,
) -> middleware::errhandlers::ErrorHandlers<B> {
    middleware::errhandlers::ErrorHandlers::default().handler(status, move |r| {
        Ok(ErrorHandlerResponse::Response(r.error_response(error())))
    })
}
