use serde::{self, de, Deserialize, Deserializer, Serialize, Serializer};
use std::{fmt, str::FromStr};

#[derive(Debug, Clone)]
pub(crate) struct MediaType(mime::Mime);

impl MediaType {
    pub(crate) fn is_compatible(&self, other: &MediaType) -> bool {
        self.0.type_() == other.0.type_() && self.0.subtype() == other.0.subtype()
    }

    pub(crate) fn as_str(&self) -> &str {
        self.0.as_ref()
    }
}

impl Default for MediaType {
    fn default() -> Self {
        MediaType(mime::APPLICATION_OCTET_STREAM)
    }
}

impl From<mime::Mime> for MediaType {
    fn from(mime: mime::Mime) -> Self {
        MediaType(mime)
    }
}

impl FromStr for MediaType {
    type Err = mime::FromStrError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(MediaType(s.parse()?))
    }
}

impl Serialize for MediaType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.0.as_ref())
    }
}

impl<'de> Deserialize<'de> for MediaType {
    fn deserialize<D>(deserializer: D) -> Result<MediaType, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct Visitor;

        impl<'de> de::Visitor<'de> for Visitor {
            type Value = MediaType;

            fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
                f.write_str("a valid mime type")
            }

            fn visit_str<E>(self, value: &str) -> Result<MediaType, E>
            where
                E: de::Error,
            {
                Ok(MediaType(value.parse().map_err(E::custom)?))
            }
        }

        deserializer.deserialize_str(Visitor)
    }
}
