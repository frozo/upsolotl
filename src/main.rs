mod config;
mod database;
mod error;
mod media_type;
mod pages;
mod users;

use crate::config::Config;
use crate::error::*;

use actix_web::http::StatusCode;
use log::*;
use std::fs::File;
use std::io::BufReader;
use structopt::StructOpt;
use users::User;
use users::Users;

use actix_web::{middleware, App, HttpServer};
use rustls::internal::pemfile;
use rustls::{NoClientAuth, ServerConfig};

#[derive(StructOpt)]
struct Opt {
    /// The log level
    #[structopt(
        short = "l",
        long = "level",
        default_value = "Info",
        parse(try_from_str)
    )]
    level: log::LevelFilter,

    #[structopt(subcommand)]
    cmd: Cmd,
}

#[derive(StructOpt)]
enum Cmd {
    Start,
    AddKey {
        #[structopt(long)]
        key: Option<String>,
        #[structopt(long)]
        email: Option<String>,
    },
    RemoveKey {
        key: String,
    },
    ListKeys,
}

#[actix_web::main]
async fn main() -> Result<(), Error> {
    sodiumoxide::init().expect("Sodiumoxide couldn't initialize");

    let opt = Opt::from_args();
    pretty_env_logger::formatted_builder()
        .filter_level(log::LevelFilter::Warn)
        .filter_module("upsolotl", opt.level)
        .init();

    let config = config::read_config()?;
    debug!("Read configuration file: {:?}", config);

    let users = if let Some(path) = config.database.user_file.clone() {
        Some(Users::read_from(path)?)
    } else {
        None
    };
    std::fs::create_dir_all(&config.database.directory)?;

    match opt.cmd {
        Cmd::Start => {
            start_server(config, users).await?;
        }
        Cmd::AddKey { key, email } => {
            if let Some(mut users) = users {
                let user = User { email };
                let key = if let Some(key) = key {
                    users.add_with_key(key, user)?
                } else {
                    users.add(user)?
                };
                log::info!("Added user with key `{}`", key);
            } else {
                log::error!("No `user_file` attribute in config!");
            }
        }
        Cmd::RemoveKey { key } => {
            if let Some(mut users) = users {
                if let Err(Error::AuthenticationFailed) = users.remove(&key) {
                    log::warn!("User with key `{}` not found!", key);
                } else {
                    log::info!("Removed user with key `{}`", key);
                }
            } else {
                log::error!("No `user_file` attribute in config!");
            }
        }
        Cmd::ListKeys => {
            if let Some(users) = users {
                for (key, user) in users.users_copied() {
                    if let Some(email) = &user.email {
                        log::info!("User with key `{}` has email `{}`.", key, email)
                    } else {
                        log::info!("User with key `{}` has no email attached.", key)
                    }
                }
            } else {
                log::error!("No `user_file` attribute in config!");
            }
        }
    }

    Ok(())
}

async fn start_server(config: Config, users: Option<Users>) -> Result<(), Error> {
    let server = {
        let config = config.clone();
        HttpServer::new(move || {
            #[allow(unused_mut)]
            let mut app = App::new()
                .configure(error::configure)
                .wrap(middleware::Logger::default())
                .wrap(middleware::Compress::default())
                .wrap(error::handle_status_code(StatusCode::NOT_FOUND, || {
                    Error::NotFound
                }))
                .data(config.clone())
                .data(users.clone())
                .service(pages::view)
                .service(pages::upload);

            #[cfg(feature = "legacy_compat")]
            {
                app = app
                    .service(pages::legacy_view_query)
                    .service(pages::legacy_view_param)
                    .service(pages::legacy_upload);
            }
            app
        })
    };

    match (&config.net.cert_file, &config.net.key_file) {
        (Some(cert), Some(key)) => {
            let mut rustls_config = ServerConfig::new(NoClientAuth::new());
            let mut cert_file = BufReader::new(File::open(cert)?);
            let mut key_file = BufReader::new(File::open(key)?);
            let cert_chain = pemfile::certs(&mut cert_file).unwrap();
            let mut keys = pemfile::rsa_private_keys(&mut key_file).unwrap();
            rustls_config
                .set_single_cert(cert_chain, keys.remove(0))
                .unwrap();

            server
                .bind_rustls(config.net.address, rustls_config)?
                .run()
                .await?;
            Ok(())
        }
        (None, None) => {
            server.bind(config.net.address)?.run().await?;
            Ok(())
        }
        (None, Some(_)) | (Some(_), None) => Err(Error::NeedKeyAndCert),
    }
}
