use crate::config::*;
use crate::error::*;
use crate::media_type::MediaType;

use actix_web::error::BlockingError;
use actix_web::web;
use serde::{Deserialize, Serialize};
use sodiumoxide::crypto::{kdf, secretbox};
use std::io::ErrorKind;
use std::{
    fs::File,
    io::{BufReader, BufWriter, Write},
    path::{Path, PathBuf},
};

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Metadata {
    pub(crate) encryption: Encryption,
    pub(crate) media_type: MediaType,
    pub(crate) uploader: Option<String>,
}

#[derive(Debug, Eq, PartialEq, Serialize, Deserialize)]
pub(crate) enum Encryption {
    // A XSalsa20-Poly1305 construction.
    V1 { tag: secretbox::Tag },
}

pub(crate) async fn read(
    config: &DatabaseConfig,
    hash: &str,
) -> Result<(Vec<u8>, Metadata), Error> {
    let main_key =
        base64::decode_config(hash, base64::URL_SAFE_NO_PAD).map_err(|_| Error::UnknownId)?;
    let main_key = kdf::Key::from_slice(&main_key).ok_or(Error::UnknownId)?;

    let id = derive_id(&main_key);
    let (mut buf, metadata) = read_internal(config, &id).await?;
    match metadata.encryption {
        Encryption::V1 { tag } => {
            let (key, nonce) = derive_keys(&main_key);
            secretbox::open_detached(&mut buf, &tag, &nonce, &key)
                .map_err(|_| Error::VerificationDidntPass)?;
        }
    }

    Ok((buf, metadata))
}

pub(crate) async fn write(
    config: &DatabaseConfig,
    media_type: MediaType,
    uploader: Option<String>,
    mut input: Vec<u8>,
) -> Result<String, Error> {
    let main_key = kdf::gen_key();

    let id = derive_id(&main_key);
    let (key, nonce) = derive_keys(&main_key);
    let tag = secretbox::seal_detached(&mut input, &nonce, &key);
    let metadata = Metadata {
        encryption: Encryption::V1 { tag },
        media_type,
        uploader,
    };
    write_internal(config, metadata, &id, input).await?;

    let hash = base64::encode_config(&main_key.0, base64::URL_SAFE_NO_PAD);
    Ok(hash)
}

async fn write_internal(
    config: &DatabaseConfig,
    metadata: Metadata,
    id: &str,
    input: Vec<u8>,
) -> Result<(), Error> {
    let data_path = id_to_path(&config.directory, &id)?;
    let mut meta_path = data_path.clone();
    meta_path.set_extension("meta");

    block(move || {
        std::fs::create_dir_all(meta_path.parent().expect("somehow there's no parent path"))?;
        let meta_file = BufWriter::new(File::create(&meta_path)?);
        serde_cbor::to_writer(meta_file, &metadata)?;

        let mut data_file = BufWriter::new(File::create(&data_path)?);
        data_file.write_all(&input)?;

        Ok(())
    })
    .await
}

async fn read_internal(config: &DatabaseConfig, id: &str) -> Result<(Vec<u8>, Metadata), Error> {
    let data_path = id_to_path(&config.directory, &id)?;
    let mut meta_path = data_path.clone();
    meta_path.set_extension("meta");

    block(move || {
        let meta_file = match File::open(&meta_path) {
            Ok(meta_file) => meta_file,
            Err(err) if err.kind() == ErrorKind::NotFound => return Err(Error::UnknownId),
            Err(err) => return Err(Error::Io(err)),
        };
        let meta_file = BufReader::new(meta_file);
        let metadata = serde_cbor::from_reader(meta_file)?;

        let data = std::fs::read(&data_path)?;
        Ok((data, metadata))
    })
    .await
}

fn id_to_path(directory: &Path, mut id: &str) -> Result<PathBuf, Error> {
    const STEP_SIZE: usize = 4;

    let mut path = PathBuf::from(directory);
    while id.len() > STEP_SIZE * 8 {
        path.push(&id[..STEP_SIZE]);
        id = &id[STEP_SIZE..];
    }
    path.push(id);
    Ok(path)
}

fn derive_id(main_key: &kdf::Key) -> String {
    let mut id = [0; 32];
    kdf::derive_from_key(&mut id, 1, *b"IMAGE ID", &main_key).unwrap();
    base64::encode_config(&id, base64::URL_SAFE_NO_PAD)
}

fn derive_keys(main_key: &kdf::Key) -> (secretbox::Key, secretbox::Nonce) {
    let mut enc_key = secretbox::Key([0; 32]);
    kdf::derive_from_key(&mut enc_key.0, 2, *b"ENCRYPT!", main_key).unwrap();

    let mut nonce = secretbox::Nonce([0; 24]);
    kdf::derive_from_key(&mut nonce.0, 3, *b"MY NONCE", main_key).unwrap();

    (enc_key, nonce)
}

#[cfg(feature = "legacy_compat")]
pub(crate) async fn read_legacy(
    config: &DatabaseConfig,
    name: &str,
    password: String,
) -> Result<(Vec<u8>, MediaType), Error> {
    // yep, a constant salt
    const SALT: &[u8] = b"b9*vfxUT.(qv5vb=d,+Gv?nyMZN>WTU[";
    const ITERATIONS: usize = 65536;
    // and a constant IV
    const IV: &[u8] = b"L'JX_j;n9RM!?(Ks";

    // check if the name is valid
    for c in name.chars() {
        if !matches!(c, 'a'..='z' | 'A'..='Z' | '0'..='9' | '.') {
            return Err(Error::UnknownId);
        }
    }

    let path = config.legacy_directory.join(name);
    // do some more checks against `..` and the likes, just to be sure
    if name.is_empty() || path.file_name().is_none() {
        return Err(Error::UnknownId);
    }

    block(move || {
        let buf = match std::fs::read(&path) {
            Ok(buf) => buf,
            Err(err) if err.kind() == ErrorKind::NotFound => return Err(Error::UnknownId),
            Err(err) => return Err(Error::Io(err)),
        };

        let mut key = [0; 32];
        openssl::pkcs5::pbkdf2_hmac(
            password.as_bytes(),
            SALT,
            ITERATIONS,
            openssl::hash::MessageDigest::sha1(),
            &mut key,
        )?;

        let decrypted =
            openssl::symm::decrypt(openssl::symm::Cipher::aes_256_cbc(), &key, Some(&IV), &buf)?;

        let media_type = tree_magic::from_u8(&decrypted)
            .parse()
            .expect("tree_magic shouldn't produce invalid media types");
        Ok((decrypted, media_type))
    })
    .await
}

async fn block<T, F>(f: F) -> Result<T, Error>
where
    F: FnOnce() -> Result<T, Error>,
    F: Send + 'static,
    T: Send + 'static,
{
    match web::block(f).await {
        Ok(v) => Ok(v),
        Err(BlockingError::Canceled) => Err(Error::TaskCanceled),
        Err(BlockingError::Error(err)) => Err(err),
    }
}
