use std::collections::BTreeMap;
use std::sync::{Arc, RwLock};
use std::{fs::File, io, path::PathBuf};

use crate::error::Error;

#[derive(serde::Serialize, serde::Deserialize, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub(crate) struct User {
    pub(crate) email: Option<String>,
}

#[derive(Clone)]
pub(crate) struct Users {
    inner: Arc<RwLock<Inner>>,
}

struct Inner {
    path: PathBuf,
    map: BTreeMap<String, User>,
}

impl Users {
    pub(crate) fn read_from(path: PathBuf) -> Result<Users, Error> {
        let map = match File::open(&path) {
            Ok(file) => serde_cbor::from_reader(file)?,
            Err(ref err) if err.kind() == io::ErrorKind::NotFound => BTreeMap::new(),
            Err(err) => return Err(Error::Io(err)),
        };
        Ok(Users {
            inner: Arc::new(RwLock::new(Inner { path, map })),
        })
    }

    pub(crate) fn is_verified(&self, key: &str) -> bool {
        self.inner
            .read()
            .expect("couldn't lock map")
            .map
            .contains_key(key)
    }

    pub(crate) fn add(&mut self, user: User) -> Result<String, Error> {
        let mut key = [0; 32];
        sodiumoxide::randombytes::randombytes_into(&mut key);
        let key = base64::encode_config(&key, base64::URL_SAFE_NO_PAD);
        self.add_with_key(key, user)
    }

    pub(crate) fn add_with_key(&mut self, key: String, user: User) -> Result<String, Error> {
        let mut inner = self.inner.write().expect("couldn't lock map");
        inner.map.insert(key.clone(), user);
        inner.save_to_disk()?;
        Ok(key)
    }

    pub(crate) fn remove(&mut self, key: &str) -> Result<(), Error> {
        let mut inner = self.inner.write().expect("couldn't lock map");
        if inner.map.remove(key).is_some() {
            inner.save_to_disk()?;
            Ok(())
        } else {
            // just a placeholder for the cli
            Err(Error::AuthenticationFailed)
        }
    }

    pub(crate) fn users_copied(&self) -> Vec<(String, User)> {
        let inner = self.inner.read().expect("couldn't lock map");
        inner
            .map
            .iter()
            .map(|(key, user)| (key.clone(), user.clone()))
            .collect()
    }
}

impl Inner {
    fn save_to_disk(&self) -> Result<(), Error> {
        serde_cbor::to_writer(File::create(&self.path)?, &self.map)?;
        Ok(())
    }
}
