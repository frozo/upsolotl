# Upsolotl

A server to provide secure and private image uploading and viewing.

## Endpoints

- `POST /v1/api/upload`: Upload a file which will be encrypted on the server side.
  `Content-Type` has to be `multipart/form-data`.
  `Authorization` has to be `Key {user-key}` if the server requires authentication to upload.
  Only the first multipart field will be stored. The `Content-Type` of the field will be the one that's returned to the user later on if it's compatible with the whitelist in the server configuration.
  The response will be a JSON object:
  ```json
  {
    "location": "/v1/{hash}"
  }
  ```
  `location` is the path relative to the host.
- `GET /v1/{hash}`: View a file which will be decrypted on the server side.
